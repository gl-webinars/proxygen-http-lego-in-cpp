set (TARGET_NAME "HTTPClient")

include (${TARGET_NAME}_sources.cmake)

add_library (${TARGET_NAME} STATIC
    ${SRC_HTTPCLIENT}
    ${HDR_HTTPCLIENT}
    "${TARGET_NAME}_sources.cmake"
)

set (TARGET_INCLUDES
    ${HTTPCLIENT_ROOT_BASE}/include
)

target_include_directories (${TARGET_NAME} PUBLIC ${TARGET_INCLUDES})

target_link_libraries (${TARGET_NAME} PUBLIC
    proxygen::proxygen
)

add_executable (${TARGET_NAME}Main
    ${HTTPCLIENT_ALL_SOURCES}
    ${ECHOSERVER_ALL_HEADERS}
    "${TARGET_NAME}_sources.cmake"
)

target_link_libraries (${TARGET_NAME}Main PUBLIC
    proxygen::proxygen
    proxygen::proxygenhttpserver
    HTTPClient
)
