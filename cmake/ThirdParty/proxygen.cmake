set (PROXYGEN_ROOT "${EXTERNAL_ROOT}/proxygen")

set (GLOG_ROOT "${PROXYGEN_ROOT}/glog/lib64")
set (GFLAGS_ROOT "${PROXYGEN_ROOT}/gflags/lib")
set (LIBEVENT_ROOT "${PROXYGEN_ROOT}/libevent/lib")
set (LIBSODIUM_ROOT "${PROXYGEN_ROOT}/libsodium")

set (ZSTD_ROOT_DIR "${PROXYGEN_ROOT}/zstd")

list (APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/ThirdParty")
list (APPEND CMAKE_PREFIX_PATH "${PROXYGEN_ROOT}/proxygen/lib/cmake/proxygen")
list (APPEND CMAKE_PREFIX_PATH "${PROXYGEN_ROOT}/folly/lib/cmake/folly")
list (APPEND CMAKE_PREFIX_PATH "${PROXYGEN_ROOT}/boost/lib/cmake/Boost-1.78.0")
list (APPEND CMAKE_PREFIX_PATH "${PROXYGEN_ROOT}/wangle/lib/cmake/wangle")
list (APPEND CMAKE_PREFIX_PATH "${PROXYGEN_ROOT}/mvfst/lib/cmake/mvfst")
list (APPEND CMAKE_PREFIX_PATH "${PROXYGEN_ROOT}/fizz/lib/cmake/fizz")
list (APPEND CMAKE_PREFIX_PATH "${LIBSODIUM_ROOT}")
list (APPEND CMAKE_PREFIX_PATH "${LIBEVENT_ROOT}/cmake/libevent")
list (APPEND CMAKE_PREFIX_PATH "${PROXYGEN_ROOT}/snappy/lib64/cmake/Snappy")
list (APPEND CMAKE_PREFIX_PATH "${PROXYGEN_ROOT}/fmt/lib64/cmake/fmt")
list (APPEND CMAKE_PREFIX_PATH "${GFLAGS_ROOT}/cmake/gflags")
list (APPEND CMAKE_PREFIX_PATH "${ZSTD_ROOT_DIR}/lib64/cmake/zstd")

find_package (zstd REQUIRED)
find_package (gflags REQUIRED)
find_package (proxygen REQUIRED)

include_directories ("/usr/local/openssl/include")
