set (GTEST_ROOT "${EXTERNAL_ROOT}/gtest")
if (LINUX)
    set (GTEST_ROOT "${GTEST_ROOT}-linux")
endif ()

list (APPEND CMAKE_PREFIX_PATH "${GTEST_ROOT}/lib64/cmake/GTest")

find_package (GTest REQUIRED)

# install (DIRECTORY ${GTEST_ROOT}/lib64/  
#     DESTINATION ${CMAKE_INSTALL_LIBDIR} 
#     PATTERN "cmake" EXCLUDE
#     PATTERN "pkgconfig" EXCLUDE
# )
