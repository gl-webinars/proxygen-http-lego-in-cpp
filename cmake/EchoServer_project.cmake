set (TARGET_NAME "EchoServer")

include (${TARGET_NAME}_sources.cmake)

add_executable (${TARGET_NAME}
    ${SRC_ECHOSERVER}
    ${ECHOSERVER_ALL_HEADERS}
    "${TARGET_NAME}_sources.cmake"
)

set (TARGET_INCLUDES
    ${ECHOSERVER_ROOT_BASE}/include
)

target_include_directories (${TARGET_NAME} PUBLIC ${TARGET_INCLUDES})

target_link_libraries (${TARGET_NAME} PUBLIC
    proxygen::proxygen
    proxygen::proxygenhttpserver
)

