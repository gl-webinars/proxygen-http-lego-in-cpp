set (ECHOSERVER_ROOT_BASE "${SOURCES_ROOT}/EchoServer")
set (SRC_BASE "${ECHOSERVER_ROOT_BASE}/src")
set (HDR_BASE "${ECHOSERVER_ROOT_BASE}/include")


set (HDR_ECHOSERVER
    ${HDR_BASE}/EchoHandler.h
    ${HDR_BASE}/EchoStats.h
)

set (ECHOSERVER_ALL_HEADERS
    ${HDR_ECHOSERVER}
)


set (SRC_ECHOSERVER
    ${SRC_BASE}/EchoHandler.cpp
    ${SRC_BASE}/EchoServer.cpp
)

set (ECHOSERVER_ALL_SOURCES
    ${SRC_ECHOSERVER}
)
