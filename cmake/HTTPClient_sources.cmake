set (HTTPCLIENT_ROOT_BASE "${SOURCES_ROOT}/HTTPClient")
set (SRC_BASE "${HTTPCLIENT_ROOT_BASE}/src")
set (HDR_BASE "${HTTPCLIENT_ROOT_BASE}/include")

# =============================================================================
# Header Files
#

set (HDR_MAIN
)

set (HDR_HTTPCLIENT
    ${HDR_BASE}/HTTPResponse.h
    ${HDR_BASE}/Fetch.h
)

set (HTTPCLIENT_ALL_HEADERS
    ${HDR_MAIN}
    ${HDR_HTTPCLIENT}
)

# =============================================================================
# Source Files
#

set (SRC_MAIN
    ${SRC_BASE}/FetchMain.cpp
)

set (SRC_HTTPCLIENT
    ${SRC_BASE}/Fetch.cpp
    ${SRC_BASE}/HTTPResponse.cpp
)

set (HTTPCLIENT_ALL_SOURCES
    ${SRC_MAIN}
    ${SRC_HTTPCLIENT}
)

# =============================================================================
# Groups
#

# Header Groups
source_group ("Header Files"                        FILES ${HTTPCLIENT_ALL_HEADERS})

# Source Groups
source_group ("Source Files"                        FILES ${HTTPCLIENT_ALL_SOURCES})
