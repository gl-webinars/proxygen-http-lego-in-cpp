#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "wangle::wangle" for configuration "RelWithDebInfo"
set_property(TARGET wangle::wangle APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(wangle::wangle PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "CXX"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwangle.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS wangle::wangle )
list(APPEND _IMPORT_CHECK_FILES_FOR_wangle::wangle "${_IMPORT_PREFIX}/lib/libwangle.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
