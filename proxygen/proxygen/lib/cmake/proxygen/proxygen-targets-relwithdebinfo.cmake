#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "proxygen::proxygen" for configuration "RelWithDebInfo"
set_property(TARGET proxygen::proxygen APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(proxygen::proxygen PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "CXX"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libproxygen.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS proxygen::proxygen )
list(APPEND _IMPORT_CHECK_FILES_FOR_proxygen::proxygen "${_IMPORT_PREFIX}/lib/libproxygen.a" )

# Import target "proxygen::proxygenhttpserver" for configuration "RelWithDebInfo"
set_property(TARGET proxygen::proxygenhttpserver APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(proxygen::proxygenhttpserver PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "CXX"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libproxygenhttpserver.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS proxygen::proxygenhttpserver )
list(APPEND _IMPORT_CHECK_FILES_FOR_proxygen::proxygenhttpserver "${_IMPORT_PREFIX}/lib/libproxygenhttpserver.a" )

# Import target "proxygen::proxygen_push" for configuration "RelWithDebInfo"
set_property(TARGET proxygen::proxygen_push APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(proxygen::proxygen_push PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/proxygen_push"
  )

list(APPEND _IMPORT_CHECK_TARGETS proxygen::proxygen_push )
list(APPEND _IMPORT_CHECK_FILES_FOR_proxygen::proxygen_push "${_IMPORT_PREFIX}/bin/proxygen_push" )

# Import target "proxygen::proxygen_proxy" for configuration "RelWithDebInfo"
set_property(TARGET proxygen::proxygen_proxy APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(proxygen::proxygen_proxy PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/proxygen_proxy"
  )

list(APPEND _IMPORT_CHECK_TARGETS proxygen::proxygen_proxy )
list(APPEND _IMPORT_CHECK_FILES_FOR_proxygen::proxygen_proxy "${_IMPORT_PREFIX}/bin/proxygen_proxy" )

# Import target "proxygen::proxygen_static" for configuration "RelWithDebInfo"
set_property(TARGET proxygen::proxygen_static APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(proxygen::proxygen_static PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/proxygen_static"
  )

list(APPEND _IMPORT_CHECK_TARGETS proxygen::proxygen_static )
list(APPEND _IMPORT_CHECK_FILES_FOR_proxygen::proxygen_static "${_IMPORT_PREFIX}/bin/proxygen_static" )

# Import target "proxygen::proxygen_echo" for configuration "RelWithDebInfo"
set_property(TARGET proxygen::proxygen_echo APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(proxygen::proxygen_echo PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/proxygen_echo"
  )

list(APPEND _IMPORT_CHECK_TARGETS proxygen::proxygen_echo )
list(APPEND _IMPORT_CHECK_FILES_FOR_proxygen::proxygen_echo "${_IMPORT_PREFIX}/bin/proxygen_echo" )

# Import target "proxygen::hq" for configuration "RelWithDebInfo"
set_property(TARGET proxygen::hq APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(proxygen::hq PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/hq"
  )

list(APPEND _IMPORT_CHECK_TARGETS proxygen::hq )
list(APPEND _IMPORT_CHECK_FILES_FOR_proxygen::hq "${_IMPORT_PREFIX}/bin/hq" )

# Import target "proxygen::proxygencurl" for configuration "RelWithDebInfo"
set_property(TARGET proxygen::proxygencurl APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(proxygen::proxygencurl PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "CXX"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libproxygencurl.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS proxygen::proxygencurl )
list(APPEND _IMPORT_CHECK_FILES_FOR_proxygen::proxygencurl "${_IMPORT_PREFIX}/lib/libproxygencurl.a" )

# Import target "proxygen::proxygen_curl" for configuration "RelWithDebInfo"
set_property(TARGET proxygen::proxygen_curl APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(proxygen::proxygen_curl PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/proxygen_curl"
  )

list(APPEND _IMPORT_CHECK_TARGETS proxygen::proxygen_curl )
list(APPEND _IMPORT_CHECK_FILES_FOR_proxygen::proxygen_curl "${_IMPORT_PREFIX}/bin/proxygen_curl" )

# Import target "proxygen::proxygen_h3datagram_client" for configuration "RelWithDebInfo"
set_property(TARGET proxygen::proxygen_h3datagram_client APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(proxygen::proxygen_h3datagram_client PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/proxygen_h3datagram_client"
  )

list(APPEND _IMPORT_CHECK_TARGETS proxygen::proxygen_h3datagram_client )
list(APPEND _IMPORT_CHECK_FILES_FOR_proxygen::proxygen_h3datagram_client "${_IMPORT_PREFIX}/bin/proxygen_h3datagram_client" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
