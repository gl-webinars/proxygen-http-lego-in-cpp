/* C++ code produced by gperf version 3.1 */
/* Computed positions: -k'1,8,22,$' */

#if !((' ' == 32) && ('!' == 33) && ('"' == 34) && ('#' == 35) \
      && ('%' == 37) && ('&' == 38) && ('\'' == 39) && ('(' == 40) \
      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \
      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \
      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \
      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \
      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \
      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \
      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \
      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \
      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \
      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \
      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \
      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \
      && ('Z' == 90) && ('[' == 91) && ('\\' == 92) && (']' == 93) \
      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \
      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \
      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \
      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \
      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \
      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \
      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \
      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))
/* The character set is not based on ISO-646.  */
#error "gperf generated tables don't work with this execution character set. Please report a bug to <bug-gperf@gnu.org>."
#endif


// Copyright 2015-present Facebook.  All rights reserved.

#include "proxygen/lib/http/HTTPCommonHeaders.h"
#include <cstring>
#include <folly/String.h>
#include <glog/logging.h>

namespace proxygen {

struct HTTPCommonHeaderName { const char* name; HTTPHeaderCode code; };

// Placeholder for the gen script to insert enum values alongside '%%'
// separators separating declarations, keywords, and functions.
// Note am currently unable to prevent this comment from being present in the
// output file.;
enum
  {
    TOTAL_KEYWORDS = 96,
    MIN_WORD_LENGTH = 2,
    MAX_WORD_LENGTH = 37,
    MIN_HASH_VALUE = 7,
    MAX_HASH_VALUE = 143
  };

/* maximum key range = 137, duplicates = 0 */

#ifndef GPERF_DOWNCASE
#define GPERF_DOWNCASE 1
static unsigned char gperf_downcase[256] =
  {
      0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,
     15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,
     30,  31,  32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,
     45,  46,  47,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,
     60,  61,  62,  63,  64,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106,
    107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121,
    122,  91,  92,  93,  94,  95,  96,  97,  98,  99, 100, 101, 102, 103, 104,
    105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
    120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134,
    135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149,
    150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164,
    165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179,
    180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194,
    195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209,
    210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224,
    225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
    240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254,
    255
  };
#endif

#ifndef GPERF_CASE_MEMCMP
#define GPERF_CASE_MEMCMP 1
static int
gperf_case_memcmp (const char *s1, const char *s2, size_t n)
{
  for (; n > 0;)
    {
      unsigned char c1 = gperf_downcase[(unsigned char)*s1++];
      unsigned char c2 = gperf_downcase[(unsigned char)*s2++];
      if (c1 == c2)
        {
          n--;
          continue;
        }
      return (int)c1 - (int)c2;
    }
  return 0;
}
#endif

class HTTPCommonHeadersInternal
{
private:
  static inline unsigned int hash (const char *str, size_t len);
public:
  static const struct HTTPCommonHeaderName *in_word_set (const char *str, size_t len);
};

inline unsigned int
HTTPCommonHeadersInternal::hash (const char *str, size_t len)
{
  static const unsigned char asso_values[] =
    {
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144,   7, 144, 144, 144, 144,
      144, 144, 144,   1, 144, 144, 144, 144,  63, 144,
      144, 144, 144, 144, 144,   1, 144,   5,  46,   3,
       64,  67,  23,  25, 144,  45,  27,  42,  14,  42,
       48, 144,  11,   1,   3,  62,  16,  63,  10,  41,
        4, 144, 144, 144, 144, 144, 144,   1, 144,   5,
       46,   3,  64,  67,  23,  25, 144,  45,  27,  42,
       14,  42,  48, 144,  11,   1,   3,  62,  16,  63,
       10,  41,   4, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144, 144, 144, 144, 144,
      144, 144, 144, 144, 144, 144
    };
  unsigned int hval = len;

  switch (hval)
    {
      default:
        hval += asso_values[static_cast<unsigned char>(str[21])];
      FOLLY_FALLTHROUGH;
      case 21:
      case 20:
      case 19:
      case 18:
      case 17:
      case 16:
      case 15:
      case 14:
      case 13:
      case 12:
      case 11:
      case 10:
      case 9:
      case 8:
        hval += asso_values[static_cast<unsigned char>(str[7])];
      FOLLY_FALLTHROUGH;
      case 7:
      case 6:
      case 5:
      case 4:
      case 3:
      case 2:
      case 1:
        hval += asso_values[static_cast<unsigned char>(str[0])];
        break;
    }
  return hval + asso_values[static_cast<unsigned char>(str[len - 1])];
}

static const unsigned char lengthtable[] =
  {
     3,  2,  6,  7,  6,  7,  6,  6,  5,  3,  7, 14, 11, 20,
    13, 12, 13,  7,  4, 16, 13, 15, 22, 16, 13,  8, 30,  7,
    16, 29, 32, 19, 15, 15, 22, 14, 16, 16,  3,  4, 10,  6,
    12, 17, 28, 10, 17,  4,  6,  8, 12, 15, 12,  3, 13,  5,
    15,  7,  7,  7,  4,  4, 28, 10,  8, 17, 25, 10, 29, 15,
    13, 17, 27,  5,  9, 19, 15, 16, 11, 17,  3,  9, 16, 17,
     4,  7, 12, 16, 37, 18, 13,  7,  8, 10,  9, 19
  };

static const struct HTTPCommonHeaderName wordlist[] =
  {
    {"Age", HTTP_HEADER_AGE},
    {"TE", HTTP_HEADER_TE},
    {"Accept", HTTP_HEADER_ACCEPT},
    {"Expires", HTTP_HEADER_EXPIRES},
    {"Expect", HTTP_HEADER_EXPECT},
    {"Alt-Svc", HTTP_HEADER_ALT_SVC},
    {"Cookie", HTTP_HEADER_COOKIE},
    {"Server", HTTP_HEADER_SERVER},
    {"Range", HTTP_HEADER_RANGE},
    {"Via", HTTP_HEADER_VIA},
    {"Trailer", HTTP_HEADER_TRAILER},
    {"Accept-Charset", HTTP_HEADER_ACCEPT_CHARSET},
    {"Content-MD5", HTTP_HEADER_CONTENT_MD5},
    {"Sec-WebSocket-Accept", HTTP_HEADER_SEC_WEBSOCKET_ACCEPT},
    {"Accept-Ranges", HTTP_HEADER_ACCEPT_RANGES},
    {"Content-Type", HTTP_HEADER_CONTENT_TYPE},
    {"Content-Range", HTTP_HEADER_CONTENT_RANGE},
    {"Referer", HTTP_HEADER_REFERER},
    {"Host", HTTP_HEADER_HOST},
    {"Content-Language", HTTP_HEADER_CONTENT_LANGUAGE},
    {"Authorization", HTTP_HEADER_AUTHORIZATION},
    {"X-Frame-Options", HTTP_HEADER_X_FRAME_OPTIONS},
    {"Access-Control-Max-Age", HTTP_HEADER_ACCESS_CONTROL_MAX_AGE},
    {"X-Accel-Redirect", HTTP_HEADER_X_ACCEL_REDIRECT},
    {"X-Wap-Profile", HTTP_HEADER_X_WAP_PROFILE},
    {"If-Range", HTTP_HEADER_IF_RANGE},
    {"Access-Control-Request-Headers", HTTP_HEADER_ACCESS_CONTROL_REQUEST_HEADERS},
    {"Refresh", HTTP_HEADER_REFRESH},
    {"Content-Location", HTTP_HEADER_CONTENT_LOCATION},
    {"Access-Control-Expose-Headers", HTTP_HEADER_ACCESS_CONTROL_EXPOSE_HEADERS},
    {"Access-Control-Allow-Credentials", HTTP_HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS},
    {"Content-Disposition", HTTP_HEADER_CONTENT_DISPOSITION},
    {"Accept-Language", HTTP_HEADER_ACCEPT_LANGUAGE},
    {"X-Forwarded-For", HTTP_HEADER_X_FORWARDED_FOR},
    {"X-Content-Type-Options", HTTP_HEADER_X_CONTENT_TYPE_OPTIONS},
    {"Content-Length", HTTP_HEADER_CONTENT_LENGTH},
    {"X-Requested-With", HTTP_HEADER_X_REQUESTED_WITH},
    {"X-XSS-Protection", HTTP_HEADER_X_XSS_PROTECTION},
    {"DNT", HTTP_HEADER_DNT},
    {"Date", HTTP_HEADER_DATE},
    {"Connection", HTTP_HEADER_CONNECTION},
    {"Pragma", HTTP_HEADER_PRAGMA},
    {"Edge-Control", HTTP_HEADER_EDGE_CONTROL},
    {"X-Thrift-Protocol", HTTP_HEADER_X_THRIFT_PROTOCOL},
    {"Access-Control-Allow-Headers", HTTP_HEADER_ACCESS_CONTROL_ALLOW_HEADERS},
    {"Set-Cookie", HTTP_HEADER_SET_COOKIE},
    {"Sec-WebSocket-Key", HTTP_HEADER_SEC_WEBSOCKET_KEY},
    {"Vary", HTTP_HEADER_VARY},
    {"Origin", HTTP_HEADER_ORIGIN},
    {"Location", HTTP_HEADER_LOCATION},
    {"Proxy-Status", HTTP_HEADER_PROXY_STATUS},
    {"Accept-Datetime", HTTP_HEADER_ACCEPT_DATETIME},
    {"X-Powered-By", HTTP_HEADER_X_POWERED_BY},
    {"VIP", HTTP_HEADER_VIP},
    {"If-None-Match", HTTP_HEADER_IF_NONE_MATCH},
    {"Allow", HTTP_HEADER_ALLOW},
    {"X-UA-Compatible", HTTP_HEADER_X_UA_COMPATIBLE},
    {":Status", HTTP_HEADER_COLON_STATUS},
    {"Upgrade", HTTP_HEADER_UPGRADE},
    {":Scheme", HTTP_HEADER_COLON_SCHEME},
    {"ETag", HTTP_HEADER_ETAG},
    {"Link", HTTP_HEADER_LINK},
    {"Access-Control-Allow-Methods", HTTP_HEADER_ACCESS_CONTROL_ALLOW_METHODS},
    {"User-Agent", HTTP_HEADER_USER_AGENT},
    {"If-Match", HTTP_HEADER_IF_MATCH},
    {"X-Forwarded-Proto", HTTP_HEADER_X_FORWARDED_PROTO},
    {"Strict-Transport-Security", HTTP_HEADER_STRICT_TRANSPORT_SECURITY},
    {"Keep-Alive", HTTP_HEADER_KEEP_ALIVE},
    {"Access-Control-Request-Method", HTTP_HEADER_ACCESS_CONTROL_REQUEST_METHOD},
    {"Accept-Encoding", HTTP_HEADER_ACCEPT_ENCODING},
    {"Cache-Control", HTTP_HEADER_CACHE_CONTROL},
    {"Sec-Token-Binding", HTTP_HEADER_SEC_TOKEN_BINDING},
    {"Access-Control-Allow-Origin", HTTP_HEADER_ACCESS_CONTROL_ALLOW_ORIGIN},
    {":Path", HTTP_HEADER_COLON_PATH},
    {"X-Real-IP", HTTP_HEADER_X_REAL_IP},
    {"If-Unmodified-Since", HTTP_HEADER_IF_UNMODIFIED_SINCE},
    {"Front-End-Https", HTTP_HEADER_FRONT_END_HTTPS},
    {"Content-Encoding", HTTP_HEADER_CONTENT_ENCODING},
    {"Retry-After", HTTP_HEADER_RETRY_AFTER},
    {"Transfer-Encoding", HTTP_HEADER_TRANSFER_ENCODING},
    {"P3P", HTTP_HEADER_P3P},
    {"Timestamp", HTTP_HEADER_TIMESTAMP},
    {"WWW-Authenticate", HTTP_HEADER_WWW_AUTHENTICATE},
    {"If-Modified-Since", HTTP_HEADER_IF_MODIFIED_SINCE},
    {"From", HTTP_HEADER_FROM},
    {":Method", HTTP_HEADER_COLON_METHOD},
    {"Max-Forwards", HTTP_HEADER_MAX_FORWARDS},
    {"Proxy-Connection", HTTP_HEADER_PROXY_CONNECTION},
    {"X-Content-Security-Policy-Report-Only", HTTP_HEADER_X_CONTENT_SECURITY_POLICY_REPORT_ONLY},
    {"Proxy-Authenticate", HTTP_HEADER_PROXY_AUTHENTICATE},
    {"Last-Modified", HTTP_HEADER_LAST_MODIFIED},
    {"Warning", HTTP_HEADER_WARNING},
    {"Priority", HTTP_HEADER_PRIORITY},
    {":Authority", HTTP_HEADER_COLON_AUTHORITY},
    {":Protocol", HTTP_HEADER_COLON_PROTOCOL},
    {"Proxy-Authorization", HTTP_HEADER_PROXY_AUTHORIZATION}
  };

static const signed char lookup[] =
  {
    -1, -1, -1, -1, -1, -1, -1,  0,  1, -1,  2,  3,  4,  5,
     6, -1, -1, -1,  7,  8,  9, 10, -1, 11, 12, 13, 14, 15,
    16, 17, 18, 19, 20, 21, 22, -1, 23, 24, -1, 25, 26, 27,
    28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41,
    42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
    56, 57, 58, 59, 60, -1, 61, 62, 63, 64, 65, 66, -1, 67,
    68, -1, 69, 70, 71, 72, -1, 73, 74, 75, 76, 77, -1, 78,
    79, 80, -1, -1, 81, -1, -1, 82, -1, -1, -1, 83, 84, -1,
    -1, -1, -1, -1, 85, -1, 86, -1, 87, -1, -1, -1, -1, -1,
    -1, -1, -1, 88, -1, 89, 90, -1, -1, -1, -1, 91, 92, 93,
    -1, 94, -1, 95
  };

const struct HTTPCommonHeaderName *
HTTPCommonHeadersInternal::in_word_set (const char *str, size_t len)
{
  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      unsigned int key = hash (str, len);

      if (key <= MAX_HASH_VALUE)
        {
          int index = lookup[key];

          if (index >= 0)
            {
              if (len == lengthtable[index])
                {
                  const char *s = wordlist[index].name;

                  if ((((unsigned char)*str ^ (unsigned char)*s) & ~32) == 0 && !gperf_case_memcmp (str, s, len))
                    return &wordlist[index];
                }
            }
        }
    }
  return 0;
}


HTTPHeaderCode HTTPCommonHeaders::hash(const char* name, size_t len) {
  const HTTPCommonHeaderName* match =
    HTTPCommonHeadersInternal::in_word_set(name, len);
  return (match == nullptr) ? HTTP_HEADER_OTHER : match->code;
}

std::string* HTTPCommonHeaders::initNames(
    HTTPCommonHeaderTableType type) {
  auto names = new std::string[HTTPCommonHeaders::num_codes];
  const uint8_t OFFSET = 2; // first 2 values are reserved for special cases
  for (uint64_t j = 0; j < HTTPCommonHeaders::num_codes - OFFSET; ++j) {
    uint8_t code = wordlist[j].code;
    DCHECK_EQ(names[code], std::string());
    // this would mean a duplicate code in the .gperf file
    names[code] = wordlist[j].name;
    if (type == HTTPCommonHeaderTableType::TABLE_LOWERCASE) {
      folly::toLowerAscii(const_cast<char*>(names[code].data()),
          names[code].size());
    }
  }
  return names;
}

const std::string* HTTPCommonHeaders::getPointerToTable(
    HTTPCommonHeaderTableType type) {
  // The actual  tables are static and initialized here in source
  // so as to prevent duplicate initializations that could occur through the
  // use of inline semantics or compilation unit referencing if defined in a
  // header
  switch(type) {
    case HTTPCommonHeaderTableType::TABLE_CAMELCASE:
      static const std::string* camelcaseTable = initNames(type);
      return camelcaseTable;
    case HTTPCommonHeaderTableType::TABLE_LOWERCASE:
      static const std::string* lowercaseTable = initNames(type);
      return lowercaseTable;
    default:
      // Controlled abort here so its clear from a crash stack this method
      // was called with a table type for which there is no current
      // implementation
      CHECK(false);
      return nullptr;
  }
}

} // proxygen
