/*
 * Copyright (c) Meta Platforms, Inc. and affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

// clang-format off
#pragma once

#include <cstdint>
#include <string>

#include <proxygen/lib/utils/Export.h>

namespace proxygen {

/**
 * Codes (hashes)
 */
enum HTTPHeaderCode : uint8_t {
  // Code reserved to indicate the absence of a valid field.
  HTTP_HEADER_NONE = 0,
  // Code reserved to indicate a value for which there is no perfect mapping to
  // a unique field (i.e. the default field for all other values).
  HTTP_HEADER_OTHER = 1,

  /* The following is a placeholder for the build script to generate a list
   * of enum values.
   * Note am currently unable to prevent this comment from being present in the
   * output file.
   */
  HTTP_HEADER_ACCEPT_ENCODING = 11,
  HTTP_HEADER_ACCEPT_LANGUAGE = 12,
  HTTP_HEADER_ACCEPT_RANGES = 13,
  HTTP_HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS = 14,
  HTTP_HEADER_ACCESS_CONTROL_ALLOW_HEADERS = 15,
  HTTP_HEADER_ACCESS_CONTROL_ALLOW_METHODS = 16,
  HTTP_HEADER_ACCESS_CONTROL_ALLOW_ORIGIN = 17,
  HTTP_HEADER_CONTENT_LENGTH = 31,
  HTTP_HEADER_ACCESS_CONTROL_EXPOSE_HEADERS = 18,
  HTTP_HEADER_CONTENT_LOCATION = 32,
  HTTP_HEADER_ACCESS_CONTROL_MAX_AGE = 19,
  HTTP_HEADER_CONTENT_MD5 = 33,
  HTTP_HEADER_ACCESS_CONTROL_REQUEST_HEADERS = 20,
  HTTP_HEADER_CONTENT_RANGE = 34,
  HTTP_HEADER_CONTENT_TYPE = 35,
  HTTP_HEADER_COOKIE = 36,
  HTTP_HEADER_DNT = 37,
  HTTP_HEADER_KEEP_ALIVE = 51,
  HTTP_HEADER_DATE = 38,
  HTTP_HEADER_LAST_MODIFIED = 52,
  HTTP_HEADER_ETAG = 39,
  HTTP_HEADER_LINK = 53,
  HTTP_HEADER_EDGE_CONTROL = 40,
  HTTP_HEADER_LOCATION = 54,
  HTTP_HEADER_MAX_FORWARDS = 55,
  HTTP_HEADER_ORIGIN = 56,
  HTTP_HEADER_P3P = 57,
  HTTP_HEADER_COLON_AUTHORITY = 2,
  HTTP_HEADER_SERVER = 71,
  HTTP_HEADER_PRAGMA = 58,
  HTTP_HEADER_COLON_METHOD = 3,
  HTTP_HEADER_SET_COOKIE = 72,
  HTTP_HEADER_PRIORITY = 59,
  HTTP_HEADER_COLON_PATH = 4,
  HTTP_HEADER_STRICT_TRANSPORT_SECURITY = 73,
  HTTP_HEADER_PROXY_AUTHENTICATE = 60,
  HTTP_HEADER_COLON_PROTOCOL = 5,
  HTTP_HEADER_TE = 74,
  HTTP_HEADER_COLON_SCHEME = 6,
  HTTP_HEADER_TIMESTAMP = 75,
  HTTP_HEADER_COLON_STATUS = 7,
  HTTP_HEADER_TRAILER = 76,
  HTTP_HEADER_ACCEPT = 8,
  HTTP_HEADER_TRANSFER_ENCODING = 77,
  HTTP_HEADER_ACCEPT_CHARSET = 9,
  HTTP_HEADER_X_POWERED_BY = 91,
  HTTP_HEADER_UPGRADE = 78,
  HTTP_HEADER_ACCEPT_DATETIME = 10,
  HTTP_HEADER_X_REAL_IP = 92,
  HTTP_HEADER_USER_AGENT = 79,
  HTTP_HEADER_X_REQUESTED_WITH = 93,
  HTTP_HEADER_VIP = 80,
  HTTP_HEADER_X_THRIFT_PROTOCOL = 94,
  HTTP_HEADER_X_UA_COMPATIBLE = 95,
  HTTP_HEADER_X_WAP_PROFILE = 96,
  HTTP_HEADER_X_XSS_PROTECTION = 97,
  HTTP_HEADER_ACCESS_CONTROL_REQUEST_METHOD = 21,
  HTTP_HEADER_AGE = 22,
  HTTP_HEADER_ALLOW = 23,
  HTTP_HEADER_ALT_SVC = 24,
  HTTP_HEADER_AUTHORIZATION = 25,
  HTTP_HEADER_CACHE_CONTROL = 26,
  HTTP_HEADER_CONNECTION = 27,
  HTTP_HEADER_EXPECT = 41,
  HTTP_HEADER_CONTENT_DISPOSITION = 28,
  HTTP_HEADER_EXPIRES = 42,
  HTTP_HEADER_CONTENT_ENCODING = 29,
  HTTP_HEADER_FROM = 43,
  HTTP_HEADER_CONTENT_LANGUAGE = 30,
  HTTP_HEADER_FRONT_END_HTTPS = 44,
  HTTP_HEADER_HOST = 45,
  HTTP_HEADER_IF_MATCH = 46,
  HTTP_HEADER_IF_MODIFIED_SINCE = 47,
  HTTP_HEADER_PROXY_AUTHORIZATION = 61,
  HTTP_HEADER_IF_NONE_MATCH = 48,
  HTTP_HEADER_PROXY_CONNECTION = 62,
  HTTP_HEADER_IF_RANGE = 49,
  HTTP_HEADER_PROXY_STATUS = 63,
  HTTP_HEADER_IF_UNMODIFIED_SINCE = 50,
  HTTP_HEADER_RANGE = 64,
  HTTP_HEADER_REFERER = 65,
  HTTP_HEADER_REFRESH = 66,
  HTTP_HEADER_RETRY_AFTER = 67,
  HTTP_HEADER_VARY = 81,
  HTTP_HEADER_SEC_TOKEN_BINDING = 68,
  HTTP_HEADER_VIA = 82,
  HTTP_HEADER_SEC_WEBSOCKET_ACCEPT = 69,
  HTTP_HEADER_WWW_AUTHENTICATE = 83,
  HTTP_HEADER_SEC_WEBSOCKET_KEY = 70,
  HTTP_HEADER_WARNING = 84,
  HTTP_HEADER_X_ACCEL_REDIRECT = 85,
  HTTP_HEADER_X_CONTENT_SECURITY_POLICY_REPORT_ONLY = 86,
  HTTP_HEADER_X_CONTENT_TYPE_OPTIONS = 87,
  HTTP_HEADER_X_FORWARDED_FOR = 88,
  HTTP_HEADER_X_FORWARDED_PROTO = 89,
  HTTP_HEADER_X_FRAME_OPTIONS = 90,

};

const uint8_t HTTPHeaderCodeCommonOffset = 2;

enum class HTTPCommonHeaderTableType: uint8_t {
  TABLE_CAMELCASE = 0,
  TABLE_LOWERCASE = 1,
};

class HTTPCommonHeaders {
 public:
  // Perfect hash function to match specified names
  FB_EXPORT static HTTPHeaderCode hash(const char* name, size_t len);

  FB_EXPORT inline static HTTPHeaderCode hash(const std::string& name) {
    return hash(name.data(), name.length());
  }

  FB_EXPORT static std::string* initNames(HTTPCommonHeaderTableType type);

  /* The following is a placeholder for the build script to generate a field
   * that stores the max number of defined enum fields.
   i.e. constexpr static uint64_t num_codes;
   * Note am currently unable to prevent this comment from being present in the
   * output file.
   */
  constexpr static uint64_t num_codes = 98;

  static const std::string* getPointerToTable(
    HTTPCommonHeaderTableType type);

  inline static const std::string* getPointerToName(HTTPHeaderCode code,
      HTTPCommonHeaderTableType type = HTTPCommonHeaderTableType::TABLE_CAMELCASE) {
    return getPointerToTable(type) + code;
  }

  inline static bool isNameFromTable(const std::string* headerName,
      HTTPCommonHeaderTableType type) {
    return getCodeFromTableName(headerName, type) >=
      HTTPHeaderCodeCommonOffset;
  }

  // This method supplements hash().  If dealing with string pointers, some
  // pointing to entries in the the name table and some not, this
  // method can be used in place of hash to reverse map a string from the
  // name table to its code.
  inline static HTTPHeaderCode getCodeFromTableName(
      const std::string* headerName, HTTPCommonHeaderTableType type) {
    if (headerName == nullptr) {
      return HTTP_HEADER_NONE;
    } else {
      auto diff = headerName - getPointerToTable(type);
      if (diff >= HTTPHeaderCodeCommonOffset && diff < (long)num_codes) {
        return static_cast<HTTPHeaderCode>(diff);
      } else {
        return HTTP_HEADER_OTHER;
      }
    }
  }

};

} // proxygen
// clang-format on
