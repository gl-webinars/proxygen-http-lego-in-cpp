#include "HTTPResponse.h"

namespace HTTPClient
{
folly::fbstring
HTTPResponse::getBodyAsFBString () const
{
    if (m_body)
    {
        auto unChained {m_body -> cloneCoalesced ()};
        return folly::fbstring {reinterpret_cast<const char*> (unChained -> data ()), unChained -> length ()};
    }
    return folly::fbstring {};
}

folly::fbstring
HTTPResponse::extractBodyAsFBString ()
{
    if (m_body)
    {
        return this -> extractBody () -> moveToFbString ();
    }
    return folly::fbstring {};
}

folly::dynamic
HTTPResponse::getBodyAsJson () const
{
    auto jsonString {this -> getBodyAsFBString ()};
    if (!jsonString.empty ())
    {
        return folly::parseJson (std::move (jsonString));
    }
    return folly::dynamic {};
}

folly::dynamic
HTTPResponse::extractBodyAsJson ()
{
    auto jsonString {this -> extractBodyAsFBString ()};

    if (!jsonString.empty ())
    {
        return folly::parseJson (std::move (jsonString));
    }
    return folly::dynamic {};
}

std::string
HTTPResponse::getBodyAsString () const
{
    return folly::toStdString (this -> getBodyAsFBString ());
}

std::string
HTTPResponse::extractBodyAsString ()
{
    return folly::toStdString (this -> extractBodyAsFBString ());
}
} // namespace HTTPClient