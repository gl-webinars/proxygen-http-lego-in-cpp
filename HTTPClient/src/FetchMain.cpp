#include <folly/futures/Future.h>
#include <folly/init/Init.h>
#include <folly/portability/GFlags.h>
#include <folly/ssl/Init.h>

#include "Fetch.h"

int
main (int argc, char* argv[])
{
#if FOLLY_HAVE_LIBGFLAGS
    // Enable glog logging to stderr by default.
    gflags::SetCommandLineOptionWithMode ("logtostderr", "1", gflags::SET_FLAGS_DEFAULT);
#endif

    folly::init (&argc, &argv, false);
    folly::ssl::init ();

    LOG (INFO) << "Sending request to EchoServer...";

    HTTPClient::Fetch::fetch (proxygen::HTTPMethod::POST,
                              proxygen::URL {"http://localhost:11000"},
                              proxygen::HTTPHeaders {},
                              "HelloWorld!",
                              "[FetchClientMain] ")
        .via (folly::getGlobalIOExecutor ())
        .thenTry (
            [] (auto&& responseTry)
            {
                auto& response {responseTry.value ()};
                if (response.getMessage () and response.getBody ())
                {
                    LOG (INFO) << "Got response!"
                               << "\nStatus: " << response.getMessage () -> getStatusCode ()
                               << "\nRequest-Number: " << response.getMessage () -> getHeaders ().getSingleOrEmpty ("Request-Number")
                               << "\nBody: " << response.getBodyAsString ();
                }
            })
        .thenError (folly::tag_t<std::exception> {},
                    [] (const std::exception& e)
                    {
                        LOG (ERROR) << "HttpClientMain Error: " << e.what ();
                    });

    return EXIT_SUCCESS;
}
