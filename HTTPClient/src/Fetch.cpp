#include "Fetch.h"

namespace HTTPClient
{
// static member initialisation
std::string Fetch::s_caPath {};
std::list<std::string> Fetch::s_nextProtoclosList {"h2", "h2-14", "spdy/3.1", "spdy/3", "http/1.1"};
std::string Fetch::s_certificatePath {};
std::string Fetch::s_keyPath {};

Fetch::Fetch (const proxygen::HTTPMethod httpMethod, const proxygen::URL& url, const proxygen::HTTPHeaders& headers, const std::string& logTag)
    : m_evb {folly::EventBaseManager::get () -> getEventBase ()},
      m_connector{this, proxygen::WheelTimerInstance{std::chrono::milliseconds {60000}, nullptr}}, //TODO: add setter for default timeout
      m_httpMethod {httpMethod},
      m_url {url},
      m_logTag {logTag}
{
    m_request.getHeaders () = headers;
}

proxygen::HTTPHeaders
Fetch::parseHeaders (const std::string& headersString)
{
    std::vector<folly::StringPiece> headersList;
    proxygen::HTTPHeaders headers;
    folly::split (",", headersString, headersList);
    for (const auto& headerPair : headersList)
    {
        std::vector<folly::StringPiece> nv;
        folly::split ('=', headerPair, nv);
        if (nv.size () > 0)
        {
            if (nv[0].empty ())
            {
                continue;
            }
            std::string value {""};
            for (size_t i {1}; i < nv.size (); ++i)
            {
                value += folly::to<std::string> (nv[i], '=');
            }
            if (nv.size () > 1)
            {
                value.pop_back ();
            } // trim anything else
            headers.add (nv[0], value);
        }
    }
    return headers;
}

void
Fetch::connect ()
{
    folly::SocketAddress addr {m_url.getHost (), m_url.getPort (), true};

    DLOG (INFO) << m_logTag << "Trying send request to: " << m_url.getUrl () << " ...";

    const folly::SocketOptionMap opts {
        {{SOL_SOCKET, SO_REUSEADDR}, 1}
    };

    if (m_url.isSecure ())
    {
        this -> initializeSSL ();
        m_connector.connectSSL (m_evb,
                                addr,
                                this -> getSSLContext (),
                                nullptr,
                                m_connectTimeout,
                                opts,
                                folly::AsyncSocket::anyAddress (),
                                this -> getServerName ());
    }
    else
    {
        m_connector.connect (m_evb, addr, m_connectTimeout, opts);
    }
}

folly::Future<HTTPClient::HTTPResponse>
Fetch::fetchRequest ()
{
    this -> connect ();
    return m_responsePromise.getFuture ();
}

void
Fetch::fetchRequest (folly::Promise<HTTPClient::HTTPResponse>& responsePromise)
{
    m_responsePromise = std::move (responsePromise);
    this -> connect ();
}

folly::SemiFuture<HTTPClient::HTTPResponse>
Fetch::fetch (const proxygen::HTTPMethod httpMethod,
              const proxygen::URL& url,
              const proxygen::HTTPHeaders& headers,
              const std::string& requestBody,
              const std::string& logTag)
{
    auto [responsePromise, responseFuture] {folly::makePromiseContract<HTTPClient::HTTPResponse> ()};

    // Don't explode, we should create the default global CPUExecutor lazily here.
    folly::getGlobalIOExecutor () -> add (
        [=, promise = std::move (responsePromise)] () mutable
        {
            std::unique_ptr<HTTPClient::Fetch> client {std::make_unique<HTTPClient::Fetch> (httpMethod, url, headers, logTag)};

            if (!requestBody.empty ())
            {
                client -> setRequestBody (requestBody);
            }

            client -> fetchRequest (promise);
            client -> selfDestuct (std::move (client)); // Ensure proper lifetime
        });
    return std::move (responseFuture);
}

folly::SemiFuture<HTTPClient::HTTPResponse>
Fetch::fetch (const proxygen::HTTPMethod httpMethod, const proxygen::URL& url, const proxygen::HTTPHeaders& headers, const std::string& logTag)
{
    return fetch (httpMethod, url, headers, std::string {}, logTag);
}

void
Fetch::selfDestuct (std::unique_ptr<HTTPClient::Fetch> fetchPtr)
{
    m_selfDestruct = true;
    fetchPtr.release ();
}

void
Fetch::setupSSL (const std::string& caPath, const std::string& nextProtoclos, const std::string& certificatePath, const std::string& keyPath)
{
    s_caPath          = caPath;
    s_certificatePath = certificatePath;
    s_keyPath         = keyPath;

    s_nextProtoclosList.clear ();
    folly::splitTo<std::string> (',', nextProtoclos, std::inserter (s_nextProtoclosList, s_nextProtoclosList.begin ()));
}

void
Fetch::initializeSSL ()
{
    m_sslContext = std::make_shared<folly::SSLContext> ();
    m_sslContext -> setOptions (SSL_OP_NO_COMPRESSION);
    folly::ssl::setCipherSuites<folly::ssl::SSLCommonOptions> (*(m_sslContext));
    if (!s_caPath.empty ())
    {
        m_sslContext -> loadTrustedCertificates (s_caPath.c_str ());
    }
    if (!s_certificatePath.empty () && !s_keyPath.empty ())
    {
        m_sslContext -> loadCertKeyPairFromFiles (s_certificatePath.c_str (), s_keyPath.c_str ());
    }
    m_sslContext -> setAdvertisedNextProtocols (s_nextProtoclosList);
    m_h2c = false;
}

void
Fetch::sslHandshakeFollowup (proxygen::HTTPUpstreamSession* session) noexcept
{
    folly::AsyncSSLSocket* sslSocket {dynamic_cast<folly::AsyncSSLSocket*> (session -> getTransport ())};

    const unsigned char* nextProto {nullptr};
    unsigned nextProtoLength {0};
    sslSocket -> getSelectedNextProtocol (&nextProto, &nextProtoLength);
    if (nextProto)
    {
        DLOG (INFO) << m_logTag << "Client selected next protocol " << std::string ((const char*)nextProto, nextProtoLength) << ".";
    }
    else
    {
        DLOG (INFO) << m_logTag << "Client did not select a next protocol.";
    }
}

void
Fetch::connectSuccess (proxygen::HTTPUpstreamSession* session)
{
    if (m_url.isSecure ())
    {
        this -> sslHandshakeFollowup (session);
    }
    session -> setFlowControl (m_receiveWindow, m_receiveWindow, m_receiveWindow);
    this -> sendRequest (session -> newTransaction (this));
    session -> closeWhenIdle ();
}

void
Fetch::setupHeaders ()
{
    m_request.setMethod (m_httpMethod);
    m_request.setHTTPVersion (m_httpMajor, m_httpMinor);
    m_request.setURL (m_url.makeRelativeURL ());
    m_request.setSecure (m_url.isSecure ());
    if (m_h2c)
    {
        proxygen::HTTP2Codec::requestUpgrade (m_request);
    }
    if (!m_request.getHeaders ().getNumberOfValues (proxygen::HTTP_HEADER_USER_AGENT))
    {
        m_request.getHeaders ().add (proxygen::HTTP_HEADER_USER_AGENT, "HTTPClient::Fetcht");
    }
    if (!m_request.getHeaders ().getNumberOfValues (proxygen::HTTP_HEADER_HOST))
    {
        m_request.getHeaders ().add (proxygen::HTTP_HEADER_HOST, m_url.getHostAndPort ());
    }
    if (!m_request.getHeaders ().getNumberOfValues (proxygen::HTTP_HEADER_ACCEPT))
    {
        m_request.getHeaders ().add ("Accept", "*/*");
    }
}

void
Fetch::sendRequest (proxygen::HTTPTransaction* txn)
{
    m_txn = txn;
    this -> setupHeaders ();
    m_txn -> sendHeaders (m_request);

    if (m_httpMethod == proxygen::HTTPMethod::POST)
    {
        if (m_requestBody)
        {
            m_txn -> sendBody (std::move (m_requestBody));

            if (!m_egressPaused)
            {
                m_txn -> sendEOM ();
            }
        }
    }
    else
    {
        m_txn -> sendEOM ();
    }
}

void
Fetch::connectError (const folly::AsyncSocketException& ex)
{
    LOG (ERROR) << m_logTag << "Coudln't send request: " << m_url.getUrl () << ". Error: " << ex.what () << ".";
    if (!m_responsePromise.isFulfilled ())
    {
        m_responsePromise.setException (ex);
    }
}

void
Fetch::setTransaction (proxygen::HTTPTransaction*) noexcept
{
}

void
Fetch::detachTransaction () noexcept
{
    DLOG (INFO) << m_logTag << "Transaction finished.";
    if (m_selfDestruct)
    {
        delete this;
    }
}

void
Fetch::onHeadersComplete (std::unique_ptr<proxygen::HTTPMessage> msg) noexcept
{
    m_response.m_message = std::move (msg);
}

void
Fetch::onBody (std::unique_ptr<folly::IOBuf> chain) noexcept
{
    if (m_response.m_body)
    {
        m_response.m_body -> appendToChain (std::move (chain));
    }
    else
    {
        (m_response.m_body) = std::move (chain);
    }
}

void
Fetch::onTrailers (std::unique_ptr<proxygen::HTTPHeaders>) noexcept
{
}

void
Fetch::onEOM () noexcept
{
    if (!m_responsePromise.isFulfilled ())
    {
        m_responsePromise.setValue (std::move (m_response));
    }
}

void
Fetch::onUpgrade (proxygen::UpgradeProtocol) noexcept
{
}

void
Fetch::onError (const proxygen::HTTPException& error) noexcept
{
    LOG (ERROR) << m_logTag << "An error occurred: " << error.what () << ".";
    if (!m_responsePromise.isFulfilled ())
    {
        m_responsePromise.setException (error);
    }
}

void
Fetch::onEgressPaused () noexcept
{
    m_egressPaused = true;
}

void
Fetch::onEgressResumed () noexcept
{
    m_egressPaused = false;
}

const std::string&
Fetch::getServerName () const
{
    const std::string& res {m_request.getHeaders ().getSingleOrEmpty (proxygen::HTTP_HEADER_HOST)};
    if (res.empty ())
    {
        return m_url.getHost ();
    }
    return res;
}

} // namespace HTTPClient
