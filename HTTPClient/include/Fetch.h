#pragma once

#include "HTTPResponse.h"

#include <folly/futures/Future.h>
#include <folly/io/async/EventBaseManager.h>
#include <folly/io/async/SSLOptions.h>

#include <proxygen/lib/http/codec/HTTP2Codec.h>
#include <proxygen/lib/http/HTTPConnector.h>
#include <proxygen/lib/http/session/HTTPTransaction.h>
#include <proxygen/lib/http/session/HTTPUpstreamSession.h>
#include <proxygen/lib/utils/URL.h>
#include <proxygen/lib/utils/WheelTimerInstance.h>

namespace HTTPClient
{
class Fetch : public proxygen::HTTPConnector::Callback, public proxygen::HTTPTransactionHandler
{
public:
    Fetch (folly::HHWheelTimer* timer,
           folly::EventBase* evb,
           const proxygen::HTTPMethod httpMethod,
           const proxygen::URL& url,
           const proxygen::HTTPHeaders& headers,
           const std::string& logTag = "");

    Fetch (folly::HHWheelTimer* timer,
           const proxygen::HTTPMethod httpMethod,
           const proxygen::URL& url,
           const proxygen::HTTPHeaders& headers,
           const std::string& logTag = "");

    Fetch (const proxygen::HTTPMethod httpMethod,
           const proxygen::URL& url,
           const proxygen::HTTPHeaders& headers,
           const std::string& logTag = "");

    virtual ~Fetch () override = default;

    /**
     * @brief parseHeaders can be used to convert std::string with key=value, k1=value1, ...etc to proxygen::HTTPHeaders
     *
     * @param headersString
     * @return proxygen::HTTPHeaders
     */
    static proxygen::HTTPHeaders parseHeaders (const std::string& headersString);

    /**
     * @brief fetchRequest overload, that makes HTTP request and returns folly::Future with
     * HTTPClient::HTTPResponse.
     *
     * @return folly::Future<HTTPClient::HTTPResponse>
     */
    folly::Future<HTTPClient::HTTPResponse> fetchRequest ();

    /**
     * @brief fetchRequest overload, that accepts externally created folly:Promise as argument.
     *
     * @param responsePromise
     */
    void fetchRequest (folly::Promise<HTTPClient::HTTPResponse>& responsePromise);

    /**
     * @brief Make asynchronouse HTTP request that will NOT block current thread
     * and will return Future for HTTPClient::HTTPResponse. Uses globalIOExecutor.
     *
     * @param httpMethod
     * @param url
     * @param headers
     * @param requestBody
     * @param logTag
     * @return folly::Future<HTTPClient::HTTPResponse>
     */
    static folly::SemiFuture<HTTPClient::HTTPResponse> fetch (const proxygen::HTTPMethod httpMethod,
                                                              const proxygen::URL& url,
                                                              const proxygen::HTTPHeaders& headers,
                                                              const std::string& requestBody,
                                                              const std::string& logTag);
    /**
     * @brief Make asynchronouse HTTP request that will NOT block current thread
     * and will return Future for HTTPClient::HTTPResponse. Uses globalIOExecutor.
     *
     * @param httpMethod
     * @param url
     * @param headers
     * @param logTag
     * @return folly::Future<HTTPClient::HTTPResponse>
     */
    static folly::SemiFuture<HTTPClient::HTTPResponse> fetch (const proxygen::HTTPMethod httpMethod,
                                                              const proxygen::URL& url,
                                                              const proxygen::HTTPHeaders& headers,
                                                              const std::string& logTag = std::string {});

    /**
     * @brief To ensure proper lifetime of Fetch, and to free it properly,
     * transfer ownership of dynamically allocated Fetch to itself.
     *
     * @param fetchPtr std::move-ed unique_ptr to Fetch instance
     */
    void selfDestuct (std::unique_ptr<HTTPClient::Fetch> fetchPtr);

    /**
     * @brief  Setup basic common for all Fetch instances SSL configuration.
       All parameters has reasonable defaults, so no need to change anything
       on default configurations. It is not thread safe and needs to be done as
       pre-configuration at application start.
     *
     * @param caPath Certificate Authory Certificate path
     * @param nextProtoclos Comma separated string of next acceptable protoclos
     * @param certificatePath Client certificate path
     * @param keyPath Client key path
     */
    static void setupSSL (const std::string& caPath,
                          const std::string& nextProtoclos,
                          const std::string& certificatePath = "",
                          const std::string& keyPath         = "");

    // HTTPConnector methods
    void connectSuccess (proxygen::HTTPUpstreamSession* session) override;
    void connectError (const folly::AsyncSocketException& ex) override;

    // HTTPTransactionHandler methods
    void setTransaction (proxygen::HTTPTransaction* txn) noexcept override;
    void detachTransaction () noexcept override;

    void onHeadersComplete (std::unique_ptr<proxygen::HTTPMessage> msg) noexcept override;
    void onBody (std::unique_ptr<folly::IOBuf> chain) noexcept override;
    void onTrailers (std::unique_ptr<proxygen::HTTPHeaders> trailers) noexcept override;
    void onEOM () noexcept override;
    void onUpgrade (proxygen::UpgradeProtocol protocol) noexcept override;
    void onError (const proxygen::HTTPException& error) noexcept override;
    void onEgressPaused () noexcept override;
    void onEgressResumed () noexcept override;

    // Getters
    const folly::SSLContextPtr& getSSLContext () const { return m_sslContext; }

    const std::string& getServerName () const;

    // Setters
    void setFlowControlSettings (int32_t receiveWindow) { m_receiveWindow = receiveWindow; }

    void setConnectTimeout (std::chrono::milliseconds connectTimeout) { m_connectTimeout = connectTimeout; }

    void setHTTP2Upgrade (bool h2c) { m_h2c = h2c; }

    void setHTTPVersion (uint8_t httpMajor, uint8_t httpMinor)
    {
        m_httpMajor = httpMajor;
        m_httpMinor = httpMinor;
    }

    void setRequestBody (const std::string& body) { m_requestBody = folly::IOBuf::copyBuffer (body); }

    void setUrl (const proxygen::URL& url) { m_url = url; }

    void setMethod (proxygen::HTTPMethod method) { m_httpMethod = method; }

    void setHeaders (const proxygen::HTTPHeaders& headers) { m_request.getHeaders () = headers; }

private:
    void initializeSSL ();
    void sslHandshakeFollowup (proxygen::HTTPUpstreamSession* session) noexcept;
    void setupHeaders ();
    void connect ();
    void sendRequest (proxygen::HTTPTransaction* txn);
    folly::EventBase* m_evb {nullptr};
    proxygen::HTTPConnector m_connector;
    proxygen::HTTPMethod m_httpMethod;
    proxygen::URL m_url;
    proxygen::HTTPMessage m_request;
    std::unique_ptr<folly::IOBuf> m_requestBody {nullptr};
    folly::SSLContextPtr m_sslContext {nullptr};
    proxygen::HTTPTransaction* m_txn {nullptr};
    HTTPClient::HTTPResponse m_response;
    folly::Promise<HTTPClient::HTTPResponse> m_responsePromise;
    int32_t m_receiveWindow {65536};
    std::chrono::milliseconds m_connectTimeout {1000};
    uint8_t m_httpMajor {1};
    uint8_t m_httpMinor {1};
    bool m_h2c {true};
    bool m_egressPaused {false};
    bool m_selfDestruct {false};
    std::string m_logTag;

    static std::string s_caPath;
    static std::string s_certificatePath;
    static std::string s_keyPath;
    static std::list<std::string> s_nextProtoclosList;
};

} // namespace HTTPClient
