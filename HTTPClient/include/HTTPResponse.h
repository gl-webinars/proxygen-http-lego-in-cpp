#pragma once

#include <folly/json.h>

#include <proxygen/lib/http/HTTPMessage.h>

namespace HTTPClient
{
class Fetch;

/**
 * @brief HTTPResponse is HTTP response from server
 * It contains HTTPMessage with HTTP headers, HTTP status codes etc, HTTP response without body
 * and IOBuf as raw HTTP response body itself.
 *
 */
class HTTPResponse
{
public:
    HTTPResponse ()                                        = default;
    HTTPResponse (const HTTPResponse& response)            = delete;
    HTTPResponse& operator= (const HTTPResponse& response) = delete;
    HTTPResponse (HTTPResponse&& response)                 = default;
    HTTPResponse& operator= (HTTPResponse&& response)      = default;
    ~HTTPResponse ()                                       = default;

    /**
     * @brief Get HTTPMessage* from response (response except body)
     *
     * @return const proxygen::HTTPMessage*
     */
    const proxygen::HTTPMessage* getMessage () const { return m_message.get (); }

    /**
     * @brief Extract HTTPMessage from response (response except body)
     * After this you can't use HTTPMessage from this class instance anymore.
     *
     * @return std::unique_ptr<proxygen::HTTPMessage>
     */
    std::unique_ptr<proxygen::HTTPMessage> extractMessage () { return std::move (m_message); }

    /**
     * @brief Get raw response body from response
     * After this you can use raw response body from this class instance again.
     *
     * @return const folly::IOBuf*
     */
    const folly::IOBuf* getBody () const { return m_body.get (); }

    /**
     * @brief Extract raw response body from response
     * After this you can't use raw response body from this class instance anymore.
     *
     * @return std::unique_ptr<folly::IOBuf>
     */
    std::unique_ptr<folly::IOBuf> extractBody () { return std::move (m_body); }

    /**
     * @brief Get folly::fbstring from raw response body, after this you can use use raw response body again.
     * If no body present in raw response, returns empty folly::fbstring.
     *
     * @return folly::fbstring
     */
    folly::fbstring getBodyAsFBString () const;

    /**
     * @brief Extract folly::fbstring from raw response body, after this you can't use raw response body anymore.
     * If no body present in raw response, returns empty folly::fbstring.
     *
     * @return folly::fbstring
     */
    folly::fbstring extractBodyAsFBString ();

    /**
     * @brief Get JSON from raw response body.
     * After this you can use raw response body from this class instance again.
     * If no body present in raw response, returns empty folly::dynamic.
     *
     * @return folly::dynamic
     */
    folly::dynamic getBodyAsJson () const;

    /**
     * @brief Extract JSON from raw response body.
     * After this you can't use raw response body from this class instance anymore.
     * If no body present in raw response, returns empty folly::dynamic.
     *
     * @return folly::dynamic
     */
    folly::dynamic extractBodyAsJson ();

    /**
     * @brief Extract std::string from raw response body, after this you can use raw response body again.
     * If no body present in raw response, returns empty std::string.
     *
     * @return std::string.
     */
    std::string getBodyAsString () const;

    /**
     * @brief Extract std::string from raw response body, after this you can't use raw response body anymore.
     * If no body present in raw response, returns empty std::string.
     *
     * @return std::string.
     */
    std::string extractBodyAsString ();

    // Setters mostly used for Tests.

    void setMessage (std::unique_ptr<proxygen::HTTPMessage> message) { m_message = std::move (message); }

    void removeMessage () { m_message.reset (); }

    void setBody (std::unique_ptr<folly::IOBuf> body) { m_body = std::move (body); }

    void removeBody () { m_body.reset (); }

private:
    std::unique_ptr<proxygen::HTTPMessage> m_message {nullptr};
    std::unique_ptr<folly::IOBuf> m_body {nullptr};

    friend class Fetch;
};
} // namespace HTTPClient