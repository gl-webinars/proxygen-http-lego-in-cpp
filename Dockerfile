FROM centos:7

RUN yum clean all && \
    yum install -y epel-release centos-release-scl && \
    yum update -y && \
    yum install -y sclo-git25.x86_64 && \
    yum install -y devtoolset-8-gcc devtoolset-8-gcc-c++ devtoolset-8-gdb && \
    yum install -y sudo nano mc python3 python-devel wget zlib-devel m4 help2man && \
    yum install -y perl pcre-devel perl-Thread-Queue.noarch perl-Data-Dumper.x86_64 && \
    yum install -y cmake3 make ninja-build libicu && \
    yum install -y valgrind which rpm-build gzip tar && \
    yum install -y openssh openssh-server openssh-clients && \
    yum clean -y all && \
    alternatives --install /usr/local/bin/cmake cmake /usr/bin/cmake3 20 --slave /usr/local/bin/ctest ctest /usr/bin/ctest3 --slave /usr/local/bin/cpack cpack /usr/bin/cpack3 --slave /usr/local/bin/ccmake ccmake /usr/bin/ccmake3 --family cmake && \
    source scl_source enable sclo-git25 && \
    source scl_source enable devtoolset-8 && \
    wget -c https://ftp.openssl.org/source/openssl-1.1.1k.tar.gz && tar xvzf openssl-1.1.1k.tar.gz && cd openssl-1.1.1k && \
    ./config shared zlib && make -j 8 && make install_sw && cd .. && rm -rf openssl-1.1.1k && rm openssl-1.1.1k.tar.gz && \
    ln -s /usr/local/lib64/libssl.so /usr/lib64/libssl.so && \
    ln -s /usr/local/lib64/libcrypto.so /usr/lib64/libcrypto.so && \
    echo "/usr/local/lib64" > /etc/ld.so.conf.d/openssl-1.1.1d.conf && \
    ldconfig -v

RUN mkdir -p mkdir /proxygenDemo/build
WORKDIR /proxygenDemo

ADD ./cmake /proxygenDemo/cmake
ADD ./EchoServer /proxygenDemo/EchoServer
ADD ./HTTPClient /proxygenDemo/HTTPClient
ADD ./proxygen /proxygenDemo/proxygen

RUN source scl_source enable devtoolset-8 && \
    cmake -G "Ninja Multi-Config" -B ./build/ -S ./cmake/ && \
    cmake --build ./build/ --config Debug

EXPOSE 11000 11001 11002

CMD ["/bin/bash"]
